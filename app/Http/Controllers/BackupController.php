<?php

namespace App\Http\Controllers;

use App\Mail\MailBackup;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;

class BackupController extends Controller
{
    public  function index()
    {
        Mail::to('yust.44@gmail.com')->send(new MailBackup());
    }
}
