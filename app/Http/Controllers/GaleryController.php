<?php

namespace App\Http\Controllers;

use App\Http\Requests\GaleryStoreRequest;
use App\Http\Services\GaleryServiceInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;

class GaleryController extends Controller
{
    public function __construct(private GaleryServiceInterface $galeryService)
    {
    }

    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $galeries = $this->galeryService->getAll();
        view('galery.index', ['galeries' => $galeries]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('galery.create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(GaleryStoreRequest $request)
    {
        $validated_request = $request->safe()->except(['_token']);
        try {
            $this->galeryService->store($validated_request);
            return redirect()->route('galery.index');
        } catch (\Throwable $e){
            Log::error($e->getMessage());
            return redirect()->route('galery.index');
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        $galery = $this->galeryService->getById($id);

        $galery->image = Storage::disk('google')->url($galery->image);

        return view('galery.view', ['galery' => $galery]);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }
}
