<?php

namespace App\Http\Services;

use App\Mail\MailBackup;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;

class BackupService implements BackupServiceInterface
{
    public function run()
    {
        $backup_path = storage_path('app');
        $command = "" . env('DB_DUMP_PATH', 'mysqldump') . " --user=s" . env('DB_USERNAME') . " --password=" . env('DB_PASSWORD') . " --host=" . env('DB_HOST') . " " . env('DB_DATABASE') . " > " . $backup_path . "/dump.sql";
        $output = null;
        $ret_value = null;
        $date = date('d-m-Y');

        try {
            exec($command, $output, $ret_value);

            if ($ret_value !== 0) {
                throw new \Exception('Failed executing mysql dump.');
            }

            Storage::disk('google')->put('backup/' . $date . '.sql', File::get($backup_path . '/dump.sql'));
            Log::info('Backup Success !');
        } catch (\Exception $th) {
            Log::warning('Backup Failed !', [$th->getMessage()]);
            Mail::to('yust.44@gmail.com')->send(new MailBackup());
        }
    }
}
