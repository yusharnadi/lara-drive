<?php

namespace App\Http\Services;

interface BackupServiceInterface
{
    public function run();
}
