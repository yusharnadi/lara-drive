<?php

namespace App\Http\Services;

use App\Http\Services\GaleryServiceInterface;
use App\Models\Galery;
use Illuminate\Support\Facades\Storage;

class GaleryService implements GaleryServiceInterface
{
    public function __construct(private Galery $model)
    {
    }

    public function getAll(): \Illuminate\Database\Eloquent\Collection
    {
        return $this->model->all();
    }

    public function store(array $attributes)
    {
       $attributes['image'] =  Storage::disk('google')->put('', $attributes['image']);
       return $this->model->create($attributes);
    }

    public function getById(int $id)
    {
        return $this->model->findOrFail($id);
    }
}
