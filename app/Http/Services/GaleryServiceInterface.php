<?php

namespace App\Http\Services;

interface GaleryServiceInterface
{
    public function getAll();
    public function getById(int $id);
    public function store(array $attributes);
}
