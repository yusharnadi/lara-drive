<?php

namespace App\Providers;

use App\Http\Services\BackupService;
use App\Http\Services\BackupServiceInterface;
use App\Http\Services\GaleryService;
use App\Http\Services\GaleryServiceInterface;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     */
    public function register(): void
    {
        $this->app->bind(GaleryServiceInterface::class, GaleryService::class);
        $this->app->bind(BackupServiceInterface::class, BackupService::class);
    }

    /**
     * Bootstrap any application services.
     */
    public function boot(): void
    {
        //
    }
}
